import numpy 
import matplotlib.pyplot as plt
from scipy.fftpack import fft, fftshift


import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt, mpld3
import glob


filename = '/home/han/Desktop/300720/Data-Collect-Raw-Yaw-HW-LPF-on/imu--2020-02-29_09_15_22_182.csv'

# read imu file 
imu = pd.read_csv(filename)  

# get time
timestamp = imu.iloc[:,1]

# roll
roll = imu[' roll'].to_numpy()
roll[roll >= 180] = roll[roll >= 180] - 360  # -180- 180    
# pitch
pitch = imu[' pitch'].to_numpy()
pitch[pitch >= 180] = pitch[pitch >= 180] - 360  # -180- 180
# yaws 
yaw = imu[' yaw'].to_numpy()
yaw[yaw >= 180] = yaw[yaw >= 180] - 360  # -180- 180

# compass x,y
compass_x = imu[' compass_x'].to_numpy()
compass_y = imu[' compass_y'].to_numpy()

compass_yaw = -np.arctan2(compass_y,compass_x) * 180 / np.pi

# # plot results 
# plt.figure()

# plt.plot(timestamp,yaw,label="yaw")
# plt.plot(timestamp,pitch,label="pitch")
# plt.plot(timestamp,roll,label="roll")
# plt.plot(timestamp,compass_yaw,label="compass yaw")
# plt.legend(loc="upper right")
# plt.grid()

# plt.show()

freq = fft(pitch)
freq = fftshift(freq)
freq = freq[int(len(freq)/2):]

bins = np.linspace(0,70,len(freq))

plt.figure()
# plt.subplot(211)
plt.plot(bins,np.real(freq))
plt.grid()
# plt.subplot(212)
# plt.plot(bins,np.imag(freq))
plt.show()