#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 11:51:37 2020

@author: han
"""

import json
import numpy as np
import folium
import glob
    
# use dialog to open folder 
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()
folder = filedialog.askdirectory()  # this is str of collection folder 
print('folder: ', folder)

# find file in folder and read
filename = glob.glob(folder + '/' + 'Analysis.json')[0]   # can only have one datafile 

with open(filename,'r') as load_f:
    data = json.load(load_f)
    del load_f
    # print(data)
    
# generate map at origin
scans = data["scans"]
orign = [0,0]
for scan in scans:
    lat = scan["scan_data"]["gps_lat"]
    lon = scan["scan_data"]["gps_lon"]
    if lat != 0.0 and lon != 0.0:
        orign = [lat,lon]
        break
del scan, lat, lon

m = folium.Map(
    location=orign, 
    zoom_start=15,
    tiles='https://api.mapbox.com/styles/v1/mapbox/outdoors-v11/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoiam9zaC13ZCIsImEiOiJjazg2cTJ3cDUwOHA5M2xsamFvOTVxYm53In0.1DlCY82a2f6sZQzvIZ6vZg',
    attr='Mapbox.com'
)

# show origin
folium.Marker(
    location=orign,
    popup='Origin',
    tooltip='Origin',
    icon=folium.Icon(color='red')
).add_to(m)

# show profiles location
profiles = data['profiles']
for profile in profiles:
    lat_lon = [profile['lat_lon']['lat'],profile['lat_lon']['lon']]
    rotation_number = profile['index'] + 1
    info = f""" <div style="font-family: courier new; color: {'crimson'}">
                {"{}".format(rotation_number)}
                </div>
            """
    # number 
    folium.Marker(
        location=lat_lon,
        icon=folium.DivIcon(info),
        popup=rotation_number,
        tooltip=rotation_number,
    ).add_to(m)
    # circle
    folium.Circle(
        radius=5,
        location=lat_lon,
        popup=rotation_number,
        color='crimson',
        fill=False,
    ).add_to(m)
    del lat_lon

#########################################################
# run as an app
import io
import sys
from PyQt5 import QtWidgets, QtWebEngineWidgets

app = QtWidgets.QApplication(sys.argv)

html = io.BytesIO()
m.save(html, close_file=False)

w = QtWebEngineWidgets.QWebEngineView()
w.setHtml(html.getvalue().decode())
w.resize(640, 480)
w.show()

sys.exit(app.exec_())

#########################################################
# run as web site 

# from flask import Flask

# app = Flask(__name__)

# @app.route('/')
# def index():
#     return m._repr_html_()

# if __name__ == '__main__':
#     app.run(debug=True)

