#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 11:18:02 2020

@author: han
"""

import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt, mpld3
import glob

import tkinter as tk
from tkinter import filedialog

def read_dji_midnight(dji):
    # read UTC midnight time from dji data 
    utcmidnight = datetime.datetime.combine(datetime.datetime.strptime(dji['datetime(utc)'].to_numpy(copy=True)[0], '%Y-%m-%d %H:%M:%S').date(), datetime.time()).timestamp()
    return utcmidnight

def read_dji_timestamps(dji):
    
    # get utc midnight
    utcmidnight = read_dji_midnight(dji)
    
    # get dji timestamp in utc
    djitimes = dji['datetime(utc)'].to_numpy(copy=True)
    djitimestamps = []    # datatime obj in UTC
    for djitime in djitimes:
        djitimestamps.append(datetime.datetime.strptime(djitime, '%Y-%m-%d %H:%M:%S').timestamp())
    del djitimes, djitime
    djitimestamps = (np.array(djitimestamps) - utcmidnight) % (24*3600)
    return djitimestamps
    
def read_dji_pitch_roll_yaw(djis):

    dji_pitch_roll_yaw = np.array([[],[],[],[]])
    
    for dji in djis:
        # get dji timestamp in utc
        djitimestamps = read_dji_timestamps(dji)
        
        djipitchs = dji[' pitch(degrees)'].to_numpy(copy=True)          # get dji pitch in degree
        djirolls = dji[' roll(degrees)'].to_numpy(copy=True)            # get dji roll in degree
        djiyaws = dji[' compass_heading(degrees)'].to_numpy(copy=True)  # get dji yaw in degree
        
        djiyaws = djiyaws - 90
        
        # -180- 180
        djipitchs[djipitchs >= 180] = djipitchs[djipitchs >= 180] - 360
        djirolls[djirolls >= 180] = djirolls[djirolls >= 180] - 360
        djiyaws[djiyaws >= 180] = djiyaws[djiyaws >= 180] - 360
        
        # convert to matrix 
        pitch_roll_yaw = np.array([djitimestamps,djipitchs,djirolls,djiyaws])
        
        # concatenate
        dji_pitch_roll_yaw = np.concatenate((dji_pitch_roll_yaw,pitch_roll_yaw),axis=1)
        
    dji_pitch_roll_yaw = dji_pitch_roll_yaw[:,dji_pitch_roll_yaw[0].argsort()]
    
    return dji_pitch_roll_yaw

root = tk.Tk()
root.withdraw()
folder = filedialog.askdirectory()  # this is str of collection folder 
print('folder: ', folder)

# find file in folder and read
Airdatas = glob.glob(folder + '/' + '*Airdata.csv')       # may have more than one Airdata file

# read dji file 
djis = []
for Airdata in Airdatas:
    djis.append(pd.read_csv(Airdata, error_bad_lines=False))  # redd dji file 
del Airdata

# read dji pitch roll yaw and speed
dji_pitch_roll_yaw = read_dji_pitch_roll_yaw(djis)

# read imu file 
filename = glob.glob(folder + '/' + 'imu*.csv')[0]
imu = pd.read_csv(filename)

# get time
timestamp = imu[' time'].to_numpy()

# roll
roll = imu[' roll'].to_numpy()
roll[roll >= 180] = roll[roll >= 180] - 360  # -180- 180    
# pitch
pitch = imu[' pitch'].to_numpy()
pitch[pitch >= 180] = pitch[pitch >= 180] - 360  # -180- 180
# yaws 
yaw = imu[' yaw'].to_numpy()
yaw[yaw >= 180] = yaw[yaw >= 180] - 360  # -180- 180

# compass x,y
compass_x = imu[' compass_x'].to_numpy()
compass_y = imu[' compass_y'].to_numpy()
compass_z = imu[' compass_z'].to_numpy()

print("max compass_x:",np.max(compass_x))
print("min compass_x:",np.min(compass_x))

print("max compass_y:",np.max(compass_y))
print("min compass_y:",np.min(compass_y))

print("max compass_z:",np.max(compass_z))
print("min compass_z:",np.min(compass_z))

compass_yaw = -np.arctan2(compass_y,compass_x) * 180 / np.pi


# plt.figure()

# plt.subplot(211)
# plt.plot(dji_pitch_roll_yaw[0],dji_pitch_roll_yaw[3],label="dji yaw")
# plt.plot(dji_pitch_roll_yaw[0],dji_pitch_roll_yaw[2],label="dji roll")
# plt.plot(dji_pitch_roll_yaw[0],dji_pitch_roll_yaw[1],label="dji pitch")
# plt.legend(loc="upper right")
# plt.grid()

# plt.subplot(212)
# plt.plot(timestamp,yaw,label="yaw")
# plt.plot(timestamp,pitch,label="pitch")
# plt.plot(timestamp,roll,label="roll")
# plt.plot(timestamp,compass_yaw,label="compass yaw")
# plt.legend(loc="upper right")
# plt.grid()
# plt.show()

plt.figure()
plt.plot(timestamp,compass_x,label="compass_x")
plt.plot(timestamp,compass_y,label="compass_y")
# plt.plot(timestamp,compass_z,label="compass_z")
plt.legend(loc="upper right")
plt.grid()
plt.show()
    