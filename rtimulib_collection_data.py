#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 11:18:02 2020

@author: han
"""

import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt, mpld3
import glob

if __name__ == "__main__":

    # use dialog to open folder 
    import tkinter as tk
    from tkinter import filedialog
    root = tk.Tk()
    root.withdraw()
    filename = filedialog.askopenfile().name  # this is str of collection folder 
    print('file: ', filename)
    
    # read imu file 
    imu = pd.read_csv(filename)  
    
    # get time
    timestamp = imu.iloc[:,1]
    
    # roll
    roll = imu[' roll'].to_numpy()
    roll[roll >= 180] = roll[roll >= 180] - 360  # -180- 180    
    # pitch
    pitch = imu[' pitch'].to_numpy()
    pitch[pitch >= 180] = pitch[pitch >= 180] - 360  # -180- 180
    # yaws 
    yaw = imu[' yaw'].to_numpy()
    yaw[yaw >= 180] = yaw[yaw >= 180] - 360  # -180- 180
    
    # compass x,y
    compass_x = imu[' compass_x'].to_numpy()
    compass_y = imu[' compass_y'].to_numpy()
    
    compass_yaw = -np.arctan2(compass_y,compass_x) * 180 / np.pi
    
    # plot results 
    plt.figure()
    
    #plt.subplot(211)
    plt.plot(timestamp,yaw,label="yaw")
    plt.plot(timestamp,pitch,label="pitch")
    plt.plot(timestamp,roll,label="roll")
    plt.plot(timestamp,compass_yaw,label="compass yaw")
    plt.legend(loc="upper right")
    plt.grid()
    
    #plt.subplot(212)
    #plt.plot(timestamp,height,label="height")
    
    plt.show()
    

