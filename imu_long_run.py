import requests
import json
import time
import os

# r = requests.get('http://localhost:8000/calibration_status')
# r = requests.get('http://localhost:8000/telemetry')
# 
# 
# t = json.loads(r.text)
# print(t)

def wait_to_connect():
    while is_connected() != True:
        print("waiting for reconnecting")
        time.sleep(1)
    
def is_connected():
    r = requests.get('http://localhost:8000/status')
    t = json.loads(r.text)
    data = t['data']
    base = data['base']
    if base['connection_active'] == True and base['link_connected'] == True:
        return True
    else:
        return False

def reboot():
    print("reboot")
    r = requests.get('http://localhost:8000/issueCommand?command=payloadreboot')
    t = json.loads(r.text)
    if t['success'] == True:
        return True
    else:
        return False

def has_std_error():
    os.system('scp pi@192.168.42.1:~/kestrel/error.txt /tmp/error.txt >/dev/null')
    file = open('/tmp/error.txt', 'r')
    data = file.read()
    if len(data) > 0:
        text_file = open("output.txt", "w")
        text_file.write(data)
        text_file.close()
        return data
    else:
        return False

def calibrate():
    print("calibrate")
    r = requests.get('http://localhost:8000/issueCommand?command=calibrate')
    t = json.loads(r.text)
    if t['success'] == True:
        return True
    else:
        return False

def stopcalibration():
    print("stop calibration")
    r = requests.get('http://localhost:8000/issueCommand?command=stopcalibration')
    t = json.loads(r.text)
    if t['success'] == True:
        return True
    else:
        return False

def is_calibration_valid():
    r = requests.get('http://localhost:8000/calibration_status')
    t = json.loads(r.text)
    data = t['data']
    min_max = data['min_max']

    max_x = min_max['max_x']
    min_x = min_max['min_x']
    diff_x = max_x - min_x

    max_y = min_max['max_y']
    min_y = min_max['min_y']
    diff_y = max_y - min_y

    max_z = min_max['max_z']
    min_z = min_max['min_z']
    diff_z = max_z - min_z
    print(max_x, min_x)
    print(max_y, min_y)
    print(max_z, min_z)
    if abs(max_x) < 200 and abs(max_y) < 200 and abs(max_z) < 200 and abs(min_x) < 200 and abs(min_y) < 200 and abs(min_z) < 200:
        return True
    else:
        return False

wait_to_connect()
print("connected")

time.sleep(5)

while True:

    # calibrate 
    if not calibrate():
        print("Cannot calibrate")
        break

    time.sleep(10)

    if not is_calibration_valid():
        print("invalid calibration!")
        break

    # stop calibration
    if not stopcalibration():
        print("Cannot stop calibration")
        break
        
    time.sleep(10)

print("test stopped!")
