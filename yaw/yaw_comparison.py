#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 11:18:02 2020

@author: han
"""

import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt, mpld3
import glob

def read_dji_midnight(dji):
    # read UTC midnight time from dji data 
    utcmidnight = datetime.datetime.combine(datetime.datetime.strptime(dji['datetime(utc)'].to_numpy(copy=True)[0], '%Y-%m-%d %H:%M:%S').date(), datetime.time()).timestamp()
    return utcmidnight

def read_dji_timestamps(dji):
    
    # get utc midnight
    utcmidnight = read_dji_midnight(dji)
    
    # get dji timestamp in utc
    djitimes = dji['datetime(utc)'].to_numpy(copy=True)
    djitimestamps = []    # datatime obj in UTC
    for djitime in djitimes:
        djitimestamps.append(datetime.datetime.strptime(djitime, '%Y-%m-%d %H:%M:%S').timestamp())
    del djitimes, djitime
    djitimestamps = (np.array(djitimestamps) - utcmidnight) % (24*3600)
    return djitimestamps
    
def read_dji_pitch_roll_yaw(djis):

    dji_pitch_roll_yaw = np.array([[],[],[],[]])
    
    for dji in djis:
        # get dji timestamp in utc
        djitimestamps = read_dji_timestamps(dji)
        
        djipitchs = dji[' pitch(degrees)'].to_numpy(copy=True)          # get dji pitch in degree
        djirolls = dji[' roll(degrees)'].to_numpy(copy=True)            # get dji roll in degree
        djiyaws = dji[' compass_heading(degrees)'].to_numpy(copy=True)  # get dji yaw in degree
        
        # -180- 180
        djipitchs[djipitchs >= 180] = djipitchs[djipitchs >= 180] - 360
        djirolls[djirolls >= 180] = djirolls[djirolls >= 180] - 360
        djiyaws[djiyaws >= 180] = djiyaws[djiyaws >= 180] - 360
        
        # convert to matrix 
        pitch_roll_yaw = np.array([djitimestamps,djipitchs,djirolls,djiyaws])
        
        # concatenate
        dji_pitch_roll_yaw = np.concatenate((dji_pitch_roll_yaw,pitch_roll_yaw),axis=1)
        
    dji_pitch_roll_yaw = dji_pitch_roll_yaw[:,dji_pitch_roll_yaw[0].argsort()]
    
    return dji_pitch_roll_yaw

def read_speed_x_y_z(djis):

    dji_x_y_z = np.array([[],[],[],[]])
    
    for dji in djis:
        djitimestamps = read_dji_timestamps(dji)        # get dji timestamp in utc
        x = dji[' xSpeed(m/s)'].to_numpy(copy=True)     # get dji pitch in degree
        y = dji[' ySpeed(m/s)'].to_numpy(copy=True)     # get dji roll in degree
        z = dji[' zSpeed(m/s)'].to_numpy(copy=True)     # get dji yaw in degree
        # convert to matrix 
        xyz = np.array([djitimestamps,x,y,z])
        # concatenate
        dji_x_y_z = np.concatenate((dji_x_y_z,xyz),axis=1)
        
    dji_x_y_z = dji_x_y_z[:,dji_x_y_z[0].argsort()]
    
    return dji_x_y_z

def read_wd_yaw(wd):
    
    # get wd timestamps in UTC 
    wdtimestamps = wd['GPS UTC'].to_numpy(copy=True)
    
    # get wd yaws 
    wdyaws = (wd['yaw'].to_numpy(copy=True) + 90) % 360
    
    # -180- 180
    wdyaws[wdyaws >= 180] = wdyaws[wdyaws >= 180] - 360
    
    return np.array([wdtimestamps,wdyaws])

def error_between_yaws(wd_yaws,dji_yaws):
    error_list = []
    for t in wd_yaws:
        timestamp = t[0]
        index = np.where(dji_yaws[:,0] == timestamp)[0]
        if len(index) != 0:
            error = (t[1] - dji_yaws[index,1].mean(axis=0)) % 360
            if error > 180:
                error = error - 360
            error_list.append([int(timestamp),error])
        # else:
            # print("Warning: !!! no dji data at timestamp {} !!!\n".format(timestamp))
    error_list = np.array(error_list)
    return error_list

def wd_yaw_compare(folder):
    print('folder: ', folder)
    
    # find file in folder and read
    Datafile = glob.glob(folder + '/' + 'datafile*.csv')[0]   # can only have one datafile 
    Airdatas = glob.glob(folder + '/' + '*Airdata.csv')       # may have more than one Airdata file
    
    # read wd file 
    wd = pd.read_csv(Datafile)  
    
    # read dji file 
    djis = []
    for Airdata in Airdatas:
        djis.append(pd.read_csv(Airdata, error_bad_lines=False))  # redd dji file 
    del Airdata
    
    # read wd yaw
    wd_yaw = read_wd_yaw(wd)
    
    # read dji pitch roll yaw and speed
    dji_pitch_roll_yaw = read_dji_pitch_roll_yaw(djis)
    dji_speed_x_y_z = read_speed_x_y_z(djis)
    
    # compute error between wd yaw and dji yaw 
    error = error_between_yaws(wd_yaw.T,dji_pitch_roll_yaw[[0,3],:].T)    
    print(np.abs(error[:,1]).mean())
    print(error[:,1].mean())
    print(np.std(error[:,1]))
    
    # plot results 
    # plt.rcParams['savefig.dpi'] = 200
    # plt.rcParams['figure.dpi'] = 200
    fig = plt.figure()
    ax = fig.gca()
    # wd
    ax.plot(wd_yaw[0],wd_yaw[1],'--',label="wd")
    # dji yaw pitch roll
    ax.plot(dji_pitch_roll_yaw[0],dji_pitch_roll_yaw[3],label="dji yaw")
    ax.plot(dji_pitch_roll_yaw[0],dji_pitch_roll_yaw[2],label="dji roll")
    ax.plot(dji_pitch_roll_yaw[0],dji_pitch_roll_yaw[1],label="dji pitch")
    # dji speed x y z
    # ax.plot(dji_speed_x_y_z[0],dji_speed_x_y_z[1],label="dji speed x")
    # ax.plot(dji_speed_x_y_z[0],dji_speed_x_y_z[2],label="dji speed y")
    # ax.plot(dji_speed_x_y_z[0],dji_speed_x_y_z[3],label="dji speed z")
    # error between wd and dji yaw
    ax.plot(error[:,0],error[:,1],label="yaw error")
    ax.set_ylim([-180,180])
    ax.legend(loc="upper right")
    ax.grid()
    
    return fig

if __name__ == "__main__":

    # use dialog to open folder 
    import tkinter as tk
    from tkinter import filedialog
    root = tk.Tk()
    root.withdraw()
    folder = filedialog.askdirectory()  # this is str of collection folder 
    print('folder: ', folder)
    
    # find file in folder and read
    Datafile = glob.glob(folder + '/' + 'datafile*.csv')[0]   # can only have one datafile 
    Airdatas = glob.glob(folder + '/' + '*Airdata.csv')       # may have more than one Airdata file
    
    # read wd file 
    wd = pd.read_csv(Datafile)  
    
    # read dji file 
    djis = []
    for Airdata in Airdatas:
        djis.append(pd.read_csv(Airdata, error_bad_lines=False))  # redd dji file 
    del Airdata
    
    # read wd yaw
    wd_yaw = read_wd_yaw(wd)
    
    # read dji pitch roll yaw and speed
    dji_pitch_roll_yaw = read_dji_pitch_roll_yaw(djis)
    dji_speed_x_y_z = read_speed_x_y_z(djis)
    
    # compute error between wd yaw and dji yaw 
    error = error_between_yaws(wd_yaw.T,dji_pitch_roll_yaw[[0,3],:].T)    
    print(np.abs(error[:,1]).mean())
    print(error[:,1].mean())
    print(np.std(error[:,1]))
    
    # plot results 
    
    # plt.rcParams['savefig.dpi'] = 200
    # plt.rcParams['figure.dpi'] = 200
    fig = plt.figure()
    ax = fig.gca()
    # wd
    ax.plot(wd_yaw[0],wd_yaw[1],'--',label="wd")
    # dji yaw pitch roll
    ax.plot(dji_pitch_roll_yaw[0],dji_pitch_roll_yaw[3],label="dji yaw")
    # ax.plot(dji_pitch_roll_yaw[0],dji_pitch_roll_yaw[2],label="dji roll")
    # ax.plot(dji_pitch_roll_yaw[0],dji_pitch_roll_yaw[1],label="dji pitch")
    # dji speed x y z
    # ax.plot(dji_speed_x_y_z[0],dji_speed_x_y_z[1],label="dji speed x")
    # ax.plot(dji_speed_x_y_z[0],dji_speed_x_y_z[2],label="dji speed y")
    # ax.plot(dji_speed_x_y_z[0],dji_speed_x_y_z[3],label="dji speed z")
    # error between wd and dji yaw
    ax.plot(error[:,0],error[:,1],label="yaw error")
    ax.set_ylim([-180,180])
    ax.legend(loc="upper right")
    ax.grid()
    mpld3.save_html(fig, folder + '/yaw_comparison.html')
    
    
    
    # mpld3.show()
    

# import json
# AnalysisFile = "/home/han/Codes/testcorpus/InternalTesting/CurrentSystem/200428_Package3_SVWprerelease/collection--2020-04-28_17_06_08_701/Analysis.json"
# read analysis.json
# with open(AnalysisFile,'r') as load_f:
#     wd = json.load(load_f)
#     del load_f
#     # print(data)

# wdtimes = []
# for wdscan in wd['scans']:
#     gps_time = wdscan['scan_data']['gps_time']
#     wdtimes.append(gps_time)
# wdtimestamps = np.array(wdtimes)
# del gps_time, wdscan, wdtimes

# wdyaws = []
# for wdscan in wd['scans']:
#     yaw = wdscan['tag_signal_data'][0]['yaw_deg_by_ten'] / 10
#     wdyaws.append(yaw)
# del yaw, wdscan
# wdyaws = (np.array(wdyaws) + 90) % 360

