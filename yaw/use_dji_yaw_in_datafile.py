#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 21 14:11:08 2020

@author: han
"""

import pandas as pd
import numpy as np
import datetime
import glob
import os
from pathlib import Path
import shutil

def read_dji_midnight(dji):
    # read UTC midnight time from dji data 
    utcmidnight = datetime.datetime.combine(datetime.datetime.strptime(dji['datetime(utc)'].to_numpy(copy=True)[0], '%Y-%m-%d %H:%M:%S').date(), datetime.time()).timestamp()
    return utcmidnight

def read_dji_timestamps(dji):
    
    # get utc midnight
    utcmidnight = read_dji_midnight(dji)
    
    # get dji timestamp in utc
    djitimes = dji['datetime(utc)'].to_numpy(copy=True)
    djitimestamps = []    # datatime obj in UTC
    for djitime in djitimes:
        djitimestamps.append(datetime.datetime.strptime(djitime, '%Y-%m-%d %H:%M:%S').timestamp())
    djitimestamps = (np.array(djitimestamps) - utcmidnight) % (24*3600)
    return djitimestamps
    
def read_dji_yaw(djis):

    dji_yaw = np.array([[],[]])
    
    for dji in djis:
        # get dji timestamp in utc
        djitimestamps = read_dji_timestamps(dji)
        
        djiyaws = (dji[' compass_heading(degrees)'].to_numpy(copy=True) - 90 + 360) % 360   # get dji yaw in degree
        
        # convert to matrix 
        yaw = np.array([djitimestamps,djiyaws])
        
        # concatenate
        dji_yaw = np.concatenate((dji_yaw,yaw),axis=1)
        
    dji_yaw = dji_yaw[:,dji_yaw[0].argsort()]
    
    return dji_yaw.T

def read_wd_yaw(wd):
    
    # get wd timestamps in UTC 
    wdtimestamps = wd['GPS UTC'].to_numpy(copy=True)
    
    # get wd yaws 
    wdyaws = wd['yaw'].to_numpy(copy=True)
    
    return np.transpose(np.array([wdtimestamps,wdyaws]))


def wd_copy_with_dji_yaw(wd,djis):
    
    wd_copy = wd.copy()
    
    # read wd yaw
    wd_yaws = read_wd_yaw(wd)
    wd_yaws_copy = wd_yaws.copy()
    
    # read dji yaw
    dji_yaws = read_dji_yaw(djis)
    
    # temporary use for loop to do this (slow but work)
    for wd_yaw in wd_yaws_copy:
        timestamp = wd_yaw[0]
        index = np.where(dji_yaws[:,0] == timestamp)[0]
        if len(index) != 0:
            wd_yaw[1] = int(dji_yaws[index][:,1].mean(axis=0))
        else:
            print("Warning: !!! no dji data at timestamp {} !!!\n".format(timestamp))
    del timestamp, wd_yaw
    
    for i in range(len(wd_copy)):
        wd_copy['yaw'][i] = wd_yaws_copy[i,1]
        
    return wd_copy

def save_to_adjusted_collection_folder(folder, wd_new):
    
    # create new folder 
    base_folder = os.path.abspath(os.path.dirname(folder))
    name = os.path.basename(folder)
    if name.find('collection') >= 0:
        name = name.replace('collection','collection-adjusted')
    else: 
        name = 'adjusted--' + name
    new_folder = base_folder + '/' + name
    Path(new_folder).mkdir(parents=True, exist_ok=True)
    
    # save new datafile
    wd_new.to_csv(new_folder + '/datafile.csv', index=False)
    
    # copy other files 
    mapfile = glob.glob(folder + '/' + 'mapfile*.csv')[0]
    shutil.copyfile(mapfile,new_folder + '/mapfile.csv')
    
    plotfile = glob.glob(folder + '/' + 'plotfile*.csv')[0]
    shutil.copyfile(plotfile,new_folder + '/plotfile.csv')
    
    tags = glob.glob(folder + '/' + 'tags*.csv')[0]
    shutil.copyfile(tags,new_folder + '/tags.csv')
    
    known_tags = glob.glob(folder + '/' + 'known_tags*.csv')[0]
    shutil.copyfile(known_tags,new_folder + '/known_tags.csv')
    
def wd_yaw_replace(folder):

    print('folder: ', folder)

    # find file in folder and read
    Datafile = glob.glob(folder + '/' + 'datafile*.csv')[0]   # can only have one datafile 
    Airdatas = glob.glob(folder + '/' + '*Airdata.csv')       # may have more than one Airdata file
    
    # read wd file 
    wd = pd.read_csv(Datafile)  
    
    # read dji file 
    djis = []
    for Airdata in Airdatas:
        djis.append(pd.read_csv(Airdata, error_bad_lines=False))  # redd dji file 
    del Airdata
    
    # get new wd with dji yaw
    wd_new = wd_copy_with_dji_yaw(wd.copy(),djis.copy())
    
    # save to datafile 
    save_to_adjusted_collection_folder(folder, wd_new)
    
    
if __name__ == "__main__":
    
    # use dialog
    import tkinter as tk
    from tkinter import filedialog
    root = tk.Tk()
    root.withdraw()
    folder = filedialog.askdirectory()
    print('folder: ', folder)
    
    # folder = '/home/han/Codes/testcorpus/InternalTesting/CurrentSystem/200512_PreRelease_Pay456/200512_PreRelease_Pay4/collection--2020-05-12_12_03_11_741'
    # folder = '/home/han/Codes/testcorpus/InternalTesting/CurrentSystem/200512_PreRelease_Pay456/200512_PreRelease_Pay5/collection--2020-05-12_15_01_36_221'
    # folder = '/home/han/Codes/testcorpus/InternalTesting/CurrentSystem/200512_PreRelease_Pay456/200512_PreRelease_Pay6/collection--2020-05-12_15_43_48_370'

    # find file in folder and read
    Datafile = glob.glob(folder + '/' + 'datafile*.csv')[0]   # can only have one datafile 
    Airdatas = glob.glob(folder + '/' + '*Airdata.csv')       # may have more than one Airdata file
    
    # read wd file 
    wd = pd.read_csv(Datafile)  
    
    # read dji file 
    djis = []
    for Airdata in Airdatas:
        djis.append(pd.read_csv(Airdata, error_bad_lines=False))  # redd dji file 
    del Airdata
    
    # get new wd with dji yaw
    wd_new = wd_copy_with_dji_yaw(wd.copy(),djis.copy())
    
    # save to datafile 
    save_to_adjusted_collection_folder(folder, wd_new)
    
    # # compare the results 
    
    # import matplotlib.pyplot as plt
    
    # # read wd yaw
    # wd_yaw = read_wd_yaw(wd)
    # wd_yaw_new = read_wd_yaw(wd_new)
    
    # # read dji pitch roll yaw and speed
    # dji_yaw = read_dji_yaw(djis)
    
    # # plot results 
    # plt.rcParams['savefig.dpi'] = 200
    # plt.rcParams['figure.dpi'] = 200
    # fig = plt.figure()
    # ax = fig.gca()
    # ax.plot(dji_yaw[:,0],dji_yaw[:,1],'k',label="dji yaw")
    # ax.plot(wd_yaw[:,0],wd_yaw[:,1],'--b',label="wd")
    # ax.plot(wd_yaw_new[:,0],wd_yaw_new[:,1],'-r',label="wd copy")
    # ax.set_ylim([0,360])
    # ax.legend(loc="upper right")
    # ax.grid()
    # fig.show()
    